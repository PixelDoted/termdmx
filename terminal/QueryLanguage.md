## UNIVERSE

### GET, SET

`SET <Ind> <Val> UNIVERSE <UId>` set `<Val>` at `<Ind>` in `<UId>`  
`GET <Ind> UNIVERSE <UId>`       get `<Val>` at `<Ind>` in `<UId>`  

`SET <Ind> <Val>` -> `SET <Ind> <Val> UNIVERSE 0`  
`GET <Ind>`		  -> `GET <Ind> UNIVERSE 0`

### BLACKOUT

`BLACKOUT UNIVERSE <UId>` sets all bytes in `<UId>` to `0`
`BLACKOUT`                sets all bytes in every universe to `0`

## DEVICE

`DEVICE <DId> INSERT <Ind> <Len> <UId>` create a new device at `<Ind>` with `<Len>` in `<UId>` as `<DId>`  
`SET <Ind> <Val> DEVICE <DId>`          set `<Val>` at `<Ind>` in `<DId>`  
`GET <Ind> DEVICE <DId>`                get `<Val>` at `<Ind>` in `<DId>`

## SCENE

`SCENE <SId> CREATE`              create a new scene as `<SId>`  
`SCENE <SId> REMOVE`              remove scene `<SId>`  

`SCENE <SId> INSERT <DId>`        insert `<DId>` into `<SId>`  
`SCENE <SId> INSERT <Ind> <UId>`  insert byte `<Ind>` in `<UId>` into `<SId>`  

`SCENE <SId> REMOVE <DId>`        remove `<DId>` from `<SId>`  
`SCENE <SId> REMOVE <Ind> <UId>`  remove byte `<Ind>` in `<UId>` from `<SId>`  

`SCENE <SId>`       runs `<SId>`
`SCENE <SId> <Dur>` runs `<SId>` with fade duration `<Dur>`

## List

`LIST DEVICES` lists all devices
`LIST SCENES`  lists all scenes