use nom::{
    branch::alt,
    bytes::complete::{escaped, tag, take_while},
    character::complete::{alphanumeric1, char, one_of, u32},
    combinator::{cut, map, opt, value},
    error::{context, ContextError, ParseError},
    multi::separated_list0,
    number::complete::float,
    sequence::{delimited, preceded, terminated},
    IResult,
};

#[derive(Debug, Clone)]
pub enum Value {
    None,
    String(String),
    Bool(bool),
    U32(u32),
    F32(f32),
    Vec(Vec<Value>),
    Root(Vec<Value>),
}

fn sp<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, &'a str, E> {
    let chars = "\t\r\n";

    // nom combinators like `take_while` return a function. That function is the
    // parser,to which we can pass the input
    take_while(move |c| chars.contains(c))(i)
}

fn parse_str<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, &'a str, E> {
    escaped(alphanumeric1, '\\', one_of("\"n\\"))(i)
}

fn boolean<'a, E: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, bool, E> {
    // This is a parser that returns `true` if it sees the string "true", and
    // an error otherwise
    let parse_true = value(true, tag("true"));

    // This is a parser that returns `false` if it sees the string "false", and
    // an error otherwise
    let parse_false = value(false, tag("false"));

    // `alt` combines the two parsers. It returns the result of the first
    // successful parser, or an error
    alt((parse_true, parse_false))(input)
}

fn null<'a, E: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, (), E> {
    value((), tag("null"))(input)
}

fn string<'a, E: ParseError<&'a str> + ContextError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, &'a str, E> {
    context("string", cut(parse_str))(i)
    /*context(
        "string",
        preceded(char('\"'), cut(terminated(parse_str, char('\"')))),
    )(i)*/
}

fn array<'a, E: ParseError<&'a str> + ContextError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, Vec<Value>, E> {
    context(
        "array",
        preceded(
            char('['),
            cut(terminated(
                separated_list0(preceded(sp, char(' ')), json_value),
                preceded(sp, char(']')),
            )),
        ),
    )(i)
}

fn root_vec<'a, E: ParseError<&'a str> + ContextError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, Vec<Value>, E> {
    context(
        "array",
        cut(separated_list0(preceded(sp, char(' ')), json_value)),
    )(i)
}

/// here, we apply the space parser before trying to parse a value
fn json_value<'a, E: ParseError<&'a str> + ContextError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, Value, E> {
    preceded(
        sp,
        alt((
            map(array, Value::Vec),
            map(u32, Value::U32),
            map(float, Value::F32),
            map(boolean, Value::Bool),
            map(null, |_| Value::None),
            map(string, |s| Value::String(String::from(s))),
        )),
    )(i)
}

/// the root element of a JSON parser is either an object or an array
pub fn root<'a, E: ParseError<&'a str> + ContextError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, Value, E> {
    delimited(
        sp,
        alt((map(root_vec, Value::Root), map(null, |_| Value::None))),
        opt(sp),
    )(i)
}
