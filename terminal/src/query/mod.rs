use self::{serialize::Value, structs::*};
use nom::error::VerboseError;

mod serialize;
mod structs;

pub fn from(s: String) -> Option<String> {
    let binding = s.trim().to_lowercase();
    let split = binding.split_once(" ").unwrap_or((binding.as_str(), ""));

    let value: Value = if split.1.trim() == "" {
        Value::None
    } else {
        match serialize::root::<VerboseError<&str>>(split.1) {
            Ok(v) => v.1,
            Err(e) => {
                eprintln!("{:?}", e);
                return None;
            }
        }
    };

    let root = match value {
        Value::Root(r) => r,
        _ => Vec::new(),
    };
    let root = root.iter();

    match split.0 {
        "set" => Set::string(root),
        "get" => Get::string(root),
        "blackout" => Blackout::string(root),
        "device" => Device::string(root),
        "scene" => Scene::string(root),
        "list" => List::string(root),
        _ => None,
    }
}
