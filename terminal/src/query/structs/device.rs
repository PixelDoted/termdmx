use super::*;
use crate::query::serialize::Value;

#[derive(serde::Serialize)]
pub struct Device {
    #[serde(rename = "type")]
    pub typ: String,

    pub id: String,
    pub mode: Mode,
    pub insert: Option<Insert>,
}

#[derive(serde::Serialize)]
pub struct Insert {
    pub index: usize,
    pub length: usize,
    pub universe: Option<usize>,
}

#[derive(serde::Serialize, PartialEq)]
pub enum Mode {
    #[serde(rename = "insert")]
    Insert,
    #[serde(rename = "remove")]
    Remove,
    #[serde(rename = "get")]
    Get,
}

impl Device {
    pub fn string(mut v: std::slice::Iter<Value>) -> Option<String> {
        let id = string(v.next());
        let mode = string(v.next());
        if id.is_none() || mode.is_none() {
            return None;
        }

        let mode = match mode.unwrap().to_lowercase().as_str() {
            "insert" => Mode::Insert,
            "remove" => Mode::Remove,
            "get" => Mode::Get,
            _ => return None,
        };
        let insert = if mode == Mode::Insert {
            let index = usize(v.next());
            let length = usize(v.next());
            let universe = usize(v.next());
            if index.is_none() || length.is_none() {
                return None;
            }

            Some(Insert {
                index: index.unwrap(),
                length: length.unwrap(),
                universe,
            })
        } else {
            None
        };

        serde_json::to_string(&Device {
            typ: String::from("device"),
            id: id.unwrap(),
            mode: mode,
            insert,
        })
        .ok()
    }
}
