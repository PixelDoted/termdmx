mod blackout;
mod device;
mod get;
mod list;
mod scene;
mod set;

pub use blackout::*;
pub use device::*;
pub use get::*;
pub use list::*;
pub use scene::*;
pub use set::*;

use super::serialize::Value;

pub(super) fn usize(v: Option<&Value>) -> Option<usize> {
    if v.is_none() {
        return None;
    }

    match v.unwrap() {
        Value::U32(e) => Some(*e as usize),
        _ => None,
    }
}

pub(super) fn u32(v: Option<&Value>) -> Option<u32> {
    if v.is_none() {
        return None;
    }

    match v.unwrap() {
        Value::U32(e) => Some(*e as u32),
        _ => None,
    }
}

pub(super) fn u8(v: Option<&Value>) -> Option<u8> {
    if v.is_none() {
        return None;
    }

    match v.unwrap() {
        Value::U32(e) => Some(*e as u8),
        _ => None,
    }
}

pub(super) fn string(v: Option<&Value>) -> Option<String> {
    if v.is_none() {
        return None;
    }

    match v.unwrap() {
        Value::String(e) => Some(e.clone()),
        _ => None,
    }
}
