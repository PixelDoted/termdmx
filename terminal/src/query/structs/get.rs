use super::*;
use crate::query::serialize::Value;

#[derive(serde::Serialize)]
pub struct Get {
    #[serde(rename = "type")]
    pub typ: String,

    pub index: usize,
    pub universe: Option<usize>,
    pub device: Option<String>,
}

impl Get {
    pub fn string(mut v: std::slice::Iter<Value>) -> Option<String> {
        let index = usize(v.next());
        if index.is_none() {
            return None;
        }

        let mut universe: Option<usize> = None;
        let mut device: Option<String> = None;
        if let Some(_in) = string(v.next()) {
            match _in.to_lowercase().as_str() {
                "universe" => {
                    universe = usize(v.next());
                }
                "device" => {
                    device = string(v.next());
                }
                _ => return None,
            }
        }

        serde_json::to_string(&Get {
            typ: String::from("get"),
            index: index.unwrap(),
            universe,
            device,
        })
        .ok()
    }
}
