use super::*;
use crate::query::serialize::Value;

#[derive(serde::Serialize)]
pub struct List {
    #[serde(rename = "type")]
    pub typ: String,

    pub mode: Mode,
}

#[derive(serde::Serialize)]
pub enum Mode {
    #[serde(rename = "devices")]
    Devices,

    #[serde(rename = "scenes")]
    Scenes,
}

impl List {
    pub fn string(mut v: std::slice::Iter<Value>) -> Option<String> {
        let mode = string(v.next());
        if mode.is_none() {
            return None;
        }

        let mode = match mode.unwrap().as_str() {
            "devices" => Mode::Devices,
            "scenes" => Mode::Scenes,
            _ => return None,
        };

        serde_json::to_string(&List {
            typ: String::from("list"),
            mode,
        })
        .ok()
    }
}
