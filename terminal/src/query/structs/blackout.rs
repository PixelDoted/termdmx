use super::*;
use crate::query::serialize::Value;

#[derive(serde::Serialize)]
pub struct Blackout {
    #[serde(rename = "type")]
    pub typ: String,

    pub universe: Option<usize>,
}

impl Blackout {
    pub fn string(mut v: std::slice::Iter<Value>) -> Option<String> {
        serde_json::to_string(&Blackout {
            typ: String::from("blackout"),
            universe: usize(v.next()),
        })
        .ok()
    }
}
