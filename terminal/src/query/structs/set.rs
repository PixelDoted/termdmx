use crate::query::serialize::Value;

use super::*;

#[derive(serde::Serialize)]
pub struct Set {
    #[serde(rename = "type")]
    pub typ: String,

    pub index: usize,
    pub value: u8,
    pub universe: Option<usize>,
    pub device: Option<String>,
}

impl Set {
    pub fn string(mut v: std::slice::Iter<Value>) -> Option<String> {
        let index = usize(v.next());
        let value = u8(v.next());
        if index.is_none() || value.is_none() {
            return None;
        }

        let mut universe: Option<usize> = None;
        let mut device: Option<String> = None;
        if let Some(_in) = string(v.next()) {
            match _in.to_lowercase().as_str() {
                "universe" => {
                    universe = usize(v.next());
                }
                "device" => {
                    device = string(v.next());
                }
                _ => return None,
            }
        }

        serde_json::to_string(&Set {
            typ: String::from("set"),
            index: index.unwrap(),
            value: value.unwrap(),
            universe,
            device,
        })
        .ok()
    }
}
