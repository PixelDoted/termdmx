use super::*;
use crate::query::serialize::Value;

#[derive(serde::Serialize)]
pub struct Scene {
    #[serde(rename = "type")]
    pub typ: String,

    pub id: String,
    pub mode: Option<Mode>,

    #[serde(flatten)]
    pub item: Option<Item>,

    pub duration: Option<u32>,
}

#[derive(serde::Serialize)]
pub enum Item {
    #[serde(rename = "device")]
    Device(String),

    #[serde(rename = "byte")]
    Byte(ItemByte),
}

#[derive(serde::Serialize)]
pub struct ItemByte {
    pub index: usize,
    pub universe: Option<usize>,
}

#[derive(serde::Serialize)]
pub enum Mode {
    #[serde(rename = "create")]
    Create,

    #[serde(rename = "remove")]
    Remove,

    #[serde(rename = "insert")]
    Insert,
}

impl Scene {
    pub fn string(mut v: std::slice::Iter<Value>) -> Option<String> {
        let id = string(v.next());
        let item_1 = v.next();
        let mode = string(item_1);
        if id.is_none() {
            return None;
        }

        if mode.is_none() {
            let duration = u32(item_1);

            // Run
            return serde_json::to_string(&Scene {
                typ: String::from("scene"),
                id: id.unwrap(),
                mode: None,
                item: None,
                duration,
            })
            .ok();
        }

        let mode = match mode.unwrap().as_str() {
            "create" => Mode::Create,
            "remove" => Mode::Remove,
            "insert" => Mode::Insert,
            _ => return None,
        };

        match mode {
            // Create
            Mode::Create => serde_json::to_string(&Scene {
                typ: String::from("scene"),
                id: id.unwrap(),
                mode: Some(Mode::Create),
                item: None,
                duration: None,
            })
            .ok(),
            // Insert
            Mode::Insert => {
                let item_1 = v.next();
                if let Some(device) = string(item_1) {
                    return serde_json::to_string(&Scene {
                        typ: String::from("scene"),
                        id: id.unwrap(),
                        mode: Some(Mode::Insert),
                        item: Some(Item::Device(device)),
                        duration: None,
                    })
                    .ok();
                }

                let index = usize(item_1);
                let universe = usize(v.next());
                if index.is_none() {
                    return None;
                }

                serde_json::to_string(&Scene {
                    typ: String::from("scene"),
                    id: id.unwrap(),
                    mode: Some(Mode::Insert),
                    item: Some(Item::Byte(ItemByte {
                        index: index.unwrap(),
                        universe,
                    })),
                    duration: None,
                })
                .ok()
            }
            // Remove
            Mode::Remove => {
                let item_1 = v.next();
                if let Some(device) = string(item_1) {
                    return serde_json::to_string(&Scene {
                        typ: String::from("scene"),
                        id: id.unwrap(),
                        mode: Some(Mode::Insert),
                        item: Some(Item::Device(device)),
                        duration: None,
                    })
                    .ok();
                } else if let Some(index) = usize(item_1) {
                    let universe = usize(v.next());

                    return serde_json::to_string(&Scene {
                        typ: String::from("scene"),
                        id: id.unwrap(),
                        mode: Some(Mode::Remove),
                        item: Some(Item::Byte(ItemByte { index, universe })),
                        duration: None,
                    })
                    .ok();
                }

                serde_json::to_string(&Scene {
                    typ: String::from("scene"),
                    id: id.unwrap(),
                    mode: Some(Mode::Remove),
                    item: None,
                    duration: None,
                })
                .ok()
            }
        }
    }
}
