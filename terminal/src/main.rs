use std::error::Error;

use clap::Parser;
use colored::Colorize;
use crossbeam_channel::{Receiver, Sender};

mod query;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long, default_value_t = String::from("http://localhost:2233"))]
    url: String,

    #[arg(short, long, default_value = None)]
    username: Option<String>,

    #[arg(short, long, default_value = None)]
    password: Option<String>,
}

#[tokio::main]
async fn main() {
    let args = Args::parse();
    let (tx, rx) = crossbeam_channel::unbounded::<String>();
    let mut rl = rustyline::Editor::<()>::new().unwrap();
    let printer = rl.create_external_printer().unwrap();

    let _ = tokio::join!(
        tokio::spawn(stdin(tx, rl)),
        http(args.url, args.username, args.password, rx, printer)
    );
    // Stdin Handler
    // Socket Handler
}

async fn stdin(tx: Sender<String>, mut rl: rustyline::Editor<()>) {
    loop {
        let line = rl.readline("> ");
        if line.is_err() {
            match line.unwrap_err() {
                rustyline::error::ReadlineError::Interrupted => {
                    println!("Exited");
                    std::process::exit(0);
                }
                _ => {}
            }
            continue;
        }

        let mut request: String = String::new();
        let mut parse_error: bool = false;
        for l in line.unwrap().split(";") {
            let query = query::from(l.to_string());
            if query.is_none() {
                eprintln!("{}", format!("Parse Error: \"{}\"", l).red());
                parse_error = true;
                break;
            }

            if !request.is_empty() {
                request.push(',');
            }
            request.push_str(&query.unwrap());
        }

        if !parse_error {
            let _ = tx.send(format!("{{\"snippets\":[{}]}}", request));
        }
    }
}

async fn http(
    url: String,
    uname: Option<String>,
    pwd: Option<String>,
    rx: Receiver<String>,
    mut printer: impl rustyline::ExternalPrinter,
) {
    loop {
        let received = rx.recv();
        if received.is_err() {
            continue;
        }

        let msg = send_request(&url, received.unwrap(), &uname, &pwd).await;
        match msg {
            Ok(msg) => {
                let _ = printer.print(msg.blue().to_string());
            }
            Err(e) => {
                let error = e.source().unwrap().to_string();
                let _ = printer.print(error.red().to_string());
            }
        }
    }
}

async fn send_request(
    url: &str,
    body: String,
    uname: &Option<String>,
    pwd: &Option<String>,
) -> reqwest::Result<String> {
    let mut builder = reqwest::Client::new()
        .post(url)
        .header("Content-Type", "application/json");

    if uname.is_some() {
        builder = builder.basic_auth(uname.clone().unwrap(), pwd.clone());
    }

    builder.body(body).send().await?.text().await
}
