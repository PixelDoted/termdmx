use std::time::Duration;

const WAIT_ON_SERIAL: Duration = Duration::from_millis(3000);
const FREEUP_RESOURCES: Duration = Duration::from_millis(10);

fn main() {
    let mut port = serialport::new("/dev/ttyACM0", 115_200)
        .open()
        .expect("Failed to open port");

    let file = std::fs::OpenOptions::new()
        .read(true)
        .open("../multiverse/.memmap/0-mmap")
        .expect("Universe Mmap not found");

    let mmap = unsafe { memmap2::Mmap::map(&file) };
    if mmap.is_err() {
        return;
    }

    println!("Waiting 3 seconds for Serial Device");
    std::thread::sleep(WAIT_ON_SERIAL);
    println!("Serial Device should be Initialized");

    let mmap = mmap.unwrap();
    let mut last_mmap: Vec<u8> = vec![0; 512];
    loop {
        if &mmap[0..512] == last_mmap {
            std::thread::sleep(FREEUP_RESOURCES);
            continue;
        }

        last_mmap = mmap.to_vec();
        port.write_all(&last_mmap).unwrap();
    }
}
