# Multiverse

[report an issue](https://gitlab.com/PixelDoted/dmx-multiverse/-/issues) or
[create a pull/merge request](https://gitlab.com/PixelDoted/dmx-multiverse/-/merge_requests)  

The Multiverse controls every byte in every universe  
if you need to change one byte the Multiverse can do so in ~50 microseconds with `json`

## Universes

Every universe is writen to a Memory Mapped File in `<Multiverse Directory>/.memmap/`,  
each Memory Mapped File being named `n-mmap`,  
where `n` is a universes index  

## API

The Multiverse supports these Protocols:  
`websocket, http`  

and these Formats:  
`json, messagepack`  

# Terminal

Basic CLI helper for talking to the Multiverse

# Cereal

Example Software for using the Memory Mapped Files with an arduino  