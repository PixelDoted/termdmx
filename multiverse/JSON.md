```json
{
    // "time": 0, // Run after UTC Time (Responds after UTC Time)
    "snippets": [
        { // snippet
            "type": "", // Run Type
            ... // Run Data
        }
    ]
}
```

# Types
## Set
```json
...
{ // snippet
    "type": "set",
    "index": 0,
    "value": 255,
    // "universe": 1, defaults to '0'
    // "device": "example"
}
...
```

## Get
```json
...
{ // snippet
    "type": "get",
    "index": 0,
    // "universe": 1, defaults to '0'
    // "device": "example"
}
```

## Blackout
```json
...
{ // snippet
    "type": "blackout",
    // "universe": 1, defaults to '0'
}
```

## Device
### Insert
Creates or Updates a Device

```json
...
{ // snippet
    "type": "device",
    "id": "example",
    "mode": "insert",
    "insert": {
        "index": 0,
        "length": 8,
        // "universe": 1, defaults to '0'
    }
}
```

### Remove
```json
...
{ // snippet
    "type": "device",
    "id": "example",
    "mode": "remove"
}
```

### Get
```json
...
{ // snippet
    "type": "device",
    "id": "example",
    "mode": "get"
}
```

## Scene
### Run
```json
...
{ // snippet
    "type": "scene",
    "id": "example",
    // "duration": 0, fade duration; (Responds after Duration)
}
```

### Create
```json
...
{ // snippet
    "type": "scene",
    "id": "example",
    "mode": "create"
}
```

### Remove
```json
...
{ // snippet
    "type": "scene",
    "id": "example",
    "mode": "remove"
}
```

### Insert
```json
...
{ // snippet
    "type": "scene",
    "id": "example",
    "mode": "insert",
    "device": "example",
    /* or
    "byte": {
        "index": 0,
        // "universe": 1, // defaults to '0'
    }
    */
}
```

## List
### Devices
```json
...
{ // snippet
    "type": "list",
    "mode": "devices"
}
```

### Scenes
```json
...
{ // snippet
    "type": "list",
    "mode": "scenes"
}
```

# Example

```json
{
    "snippets": [
        {
            "type": "set",
            "index": 0,
            "value": 255
            // "universe": 0,
            // "device": "example"
        },
        {
            "type": "get",
            "index": 0
            // "universe": 0,
            // "device": "example"
        }
    ]
}
```

### response

```json
{
    "result": "ok", // parse result
    "time": "64μs", // server calculation time (average of 5)
    "snippets": [
        { // set response
            "result": "ok", // result
            "value": 255 // output
        },
        { // get response
            "result": "ok", // result
            "value": 255 // output
        }
    ]
}
```