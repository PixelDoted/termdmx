use once_cell::sync::OnceCell;

use crate::mem;

pub mod cmd;
pub mod result;

pub static mut MULTIVERSE: OnceCell<Multiverse> = OnceCell::new();

pub struct Multiverse {
    pub volatile: mem::Volatile,
    pub persistant: mem::Persistant,
}

#[derive(serde::Deserialize)]
pub struct Request {
    pub time: Option<u128>,
    pub snippets: serde_json::Value,
}

/// Parse and Handle request
pub fn parse(request: Option<Request>) -> Option<result::OverallResult> {
    if request.is_none() {
        return Some(result::OverallResult::error());
    }

    let request = request.unwrap();

    // Delay parseing request
    if let Some(time) = request.time {
        let now = std::time::UNIX_EPOCH.elapsed().unwrap().as_millis();
        let diff = (time - now) as u64;
        eprintln!("{}", diff);
        if time > now {
            std::thread::sleep(std::time::Duration::from_millis(diff));
        }
    }

    // Get multiverse
    let multiverse = unsafe { MULTIVERSE.get_mut() };
    if multiverse.is_none() {
        return None;
    }

    Some(cmd::parse_impl(request.snippets, multiverse.unwrap())) // parse request
}
