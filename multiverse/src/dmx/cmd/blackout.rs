use crate::dmx::{result::SnippetResult, Multiverse};

use super::Action;

#[derive(serde::Deserialize, Debug)]
pub struct Blackout {
    pub universe: Option<usize>,
}

/* Impls */
impl Action for Blackout {
    /// Set all bytes to '0' in all or one universe
    fn run(&self, multiverse: &mut Multiverse) -> SnippetResult {
        if self.universe.is_some() {
            multiverse.volatile[self.universe.unwrap()]
                .iter_mut()
                .for_each(|b| {
                    *b = 0;
                });
        }

        multiverse.volatile.universes.iter_mut().for_each(|u| {
            u.1.iter_mut().for_each(|b| {
                *b = 0;
            })
        });
        SnippetResult::ok(None)
    }
}
