use crate::dmx::{result::SnippetResult, Multiverse};

use super::Action;

#[derive(serde::Deserialize, Debug)]
pub struct List {
    pub mode: ListMode,
}

#[derive(serde::Deserialize, Debug)]
pub enum ListMode {
    #[serde(rename = "devices")]
    Devices, // List all Devices

    #[serde(rename = "scenes")]
    Scenes, // List all Scenes
}

/* Impls */
impl Action for List {
    // List all Devices or Scenes
    fn run(&self, multiverse: &mut Multiverse) -> SnippetResult {
        let v = match self.mode {
            ListMode::Devices => multiverse
                .persistant
                .devices
                .iter()
                .map(|(k, v)| {
                    serde_json::json!({
                        "id": k,
                        "index": v.index,
                        "length": v.length,
                        "universe": v.universe,
                    })
                })
                .collect::<Vec<serde_json::Value>>(),
            ListMode::Scenes => multiverse
                .persistant
                .scenes
                .iter()
                .map(|(k, _)| serde_json::json!({ "id": k }))
                .collect::<Vec<serde_json::Value>>(),
        };

        SnippetResult::ok(Some(serde_json::json!(v)))
    }
}
