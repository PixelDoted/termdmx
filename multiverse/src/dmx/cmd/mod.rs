mod basic;
mod blackout;
mod device;
mod list;
mod scene;

use self::{basic::Get, basic::Set, blackout::Blackout, device::Device, list::List, scene::Scene};

use super::{
    result::{OverallResult, SnippetResult},
    Multiverse,
};

/* Constants */
pub const INDEX_OUT_OF_BOUNDS: &str = "Index out of bounds.";
pub const DEVICE_DOESNT_EXSIST: &str = "Device doesn't exsist.";
pub const SCENE_DOESNT_EXSIST: &str = "Scene doesn't exsist.";

pub const INSERT_OBJECT_DOESNT_EXSIST: &str = "'insert' Object doesn't exsist.";

/* Functions */
pub fn parse_impl(json: serde_json::Value, multiverse: &mut Multiverse) -> OverallResult {
    // Parse input
    let result = serde_json::from_value::<Vec<Snippet>>(json); // parse json
    if result.is_err() {
        eprintln!("{:?}", result.unwrap_err());
        return OverallResult::error();
    }

    // Handle input
    let mut responses: Vec<SnippetResult> = Vec::new();
    for snippet in result.unwrap() {
        let response = run(snippet, multiverse); // handle request
        responses.push(response); // add response
    }

    OverallResult::ok(responses)
}

/// Handle call
fn run(snip: Snippet, multiverse: &mut Multiverse) -> SnippetResult {
    match snip.typ {
        SnippetType::Set => execute(serde_json::from_value::<Set>(snip.data), multiverse),
        SnippetType::Get => execute(serde_json::from_value::<Get>(snip.data), multiverse),
        SnippetType::Blackout => execute(serde_json::from_value::<Blackout>(snip.data), multiverse),
        SnippetType::Device => execute(serde_json::from_value::<Device>(snip.data), multiverse),
        SnippetType::Scene => execute(serde_json::from_value::<Scene>(snip.data), multiverse),
        SnippetType::List => execute(serde_json::from_value::<List>(snip.data), multiverse),
    }
}

// Handle call implementation
fn execute<T>(result: Result<T, serde_json::Error>, multiverse: &mut Multiverse) -> SnippetResult
where
    T: Action + std::fmt::Debug,
{
    if result.is_err() {
        return SnippetResult::error(Some(&result.unwrap_err().to_string()));
    }

    result.unwrap().run(multiverse)
}

/* Structs */
#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub struct Snippet {
    #[serde(rename = "type")]
    pub typ: SnippetType,
    #[serde(flatten)]
    pub data: serde_json::Value,
}

#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub enum SnippetType {
    #[serde(rename = "set")]
    Set,

    #[serde(rename = "get")]
    Get,

    #[serde(rename = "blackout")]
    Blackout,

    #[serde(rename = "device")]
    Device,

    #[serde(rename = "scene")]
    Scene,

    #[serde(rename = "list")]
    List,
}

pub trait Action {
    fn run(&self, multiverse: &mut Multiverse) -> SnippetResult;
}
