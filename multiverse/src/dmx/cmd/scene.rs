use std::{collections::HashMap, ops::Add};

use configuro::Configuro;

use crate::{
    dmx::{result::SnippetResult, Multiverse},
    dprint, mem,
};

use super::Action;

#[derive(serde::Deserialize, Debug)]
pub enum Mode {
    /// Create a Scene
    #[serde(rename = "create")]
    Create,

    // Remove a Scene, Device or Byte
    #[serde(rename = "remove")]
    Remove,

    /// Insert a Device or Byte
    #[serde(rename = "insert")]
    Insert,
}

#[derive(serde::Deserialize, Debug)]
pub enum Item {
    #[serde(rename = "device")]
    Device(String),

    #[serde(rename = "byte")]
    Byte(ItemByte),
}

#[derive(serde::Deserialize, Debug)]
pub struct ItemByte {
    pub index: usize,
    pub universe: Option<usize>,
}

/* Structs */
#[derive(serde::Deserialize, Debug)]
pub struct Scene {
    pub id: String,
    pub mode: Option<Mode>,

    #[serde(flatten)]
    pub item: Option<Item>,

    pub duration: Option<u64>,
}

/* Impls */
impl Action for Scene {
    fn run(&self, multiverse: &mut Multiverse) -> SnippetResult {
        if self.mode.is_none() {
            if !run_scene(&self.id, self.duration, multiverse) {
                dprint!("Scene: Scene doesn't exsist {}", self.id);
                return SnippetResult::error(Some(super::SCENE_DOESNT_EXSIST));
            }

            return SnippetResult::ok(None);
        }

        match self.mode.as_ref().unwrap() {
            Mode::Create => {
                // Create a Scene
                multiverse.persistant.add_scene(self.id.clone());
                dprint!("added scene '{}' to multiverse", self.id);
                SnippetResult::ok(None)
            }
            Mode::Remove => {
                if self.item.is_none() {
                    // Remove a Scene
                    multiverse.persistant.remove_scene(&self.id);
                } else {
                    // Remove a Device or Byte
                    let wrapped_scene = multiverse.persistant.scenes.get_mut(&self.id);
                    if wrapped_scene.is_none() {
                        dprint!("Scene Remove: Scene doesn't exsist {}", self.id);
                        return SnippetResult::error(Some(super::SCENE_DOESNT_EXSIST));
                    }

                    let scene = wrapped_scene.unwrap();
                    match self.item.as_ref().unwrap() {
                        // find and remove device from scene
                        Item::Device(item) => {
                            let device = multiverse.persistant.devices.get(item);
                            if device.is_none() {
                                dprint!("Scene Remove: Device doesn't exsist {}", item);
                                return SnippetResult::error(Some(super::DEVICE_DOESNT_EXSIST));
                            }

                            let device = device.unwrap();
                            let universe = scene.data.get_mut(&device.universe);
                            if universe.is_some() {
                                let universe = universe.unwrap();
                                for i in device.index..device.index + device.length {
                                    universe.remove(&i);
                                }
                            }
                        }
                        // find and remove byte from scene
                        Item::Byte(item) => {
                            let universe = scene.data.get_mut(&item.universe.unwrap_or(0));
                            if universe.is_some() {
                                let universe = universe.unwrap();
                                universe.remove(&item.index);
                            }
                        }
                    }

                    multiverse.persistant.write();
                }

                SnippetResult::ok(None)
            }
            Mode::Insert => {
                // Insert a Device or Byte
                if self.item.is_none() {
                    dprint!("Scene Insert: Insert object doesn't exsist {:?}", self.item);
                    return SnippetResult::error(Some(super::INSERT_OBJECT_DOESNT_EXSIST));
                }

                let wrapped_scene = multiverse.persistant.scenes.get_mut(&self.id);
                if wrapped_scene.is_none() {
                    dprint!("Scene Remove: Scene doesn't exsist {}", self.id);
                    return SnippetResult::error(Some(super::SCENE_DOESNT_EXSIST));
                }

                let scene = wrapped_scene.unwrap();
                match self.item.as_ref().unwrap() {
                    // Insert Device into Scene
                    Item::Device(item) => {
                        let device = multiverse.persistant.devices.get(item);
                        if device.is_none() {
                            dprint!("Scene Remove: Device doesn't exsist {}", item);
                            return SnippetResult::error(Some(super::DEVICE_DOESNT_EXSIST));
                        }

                        let device = device.unwrap();
                        let bytes = &multiverse.volatile[device.universe]
                            [device.index..device.index + device.length];
                        if !scene.data.contains_key(&device.universe) {
                            scene
                                .data
                                .insert(device.universe, std::collections::HashMap::new());
                        }

                        let universe = scene.data.get_mut(&device.universe).unwrap();
                        for i in 0..device.length {
                            universe.insert(i + device.index, bytes[i]);
                        }
                    }
                    // Insert Byte into Scene
                    Item::Byte(item) => {
                        let uid = item.universe.unwrap_or(0);
                        if !scene.data.contains_key(&uid) {
                            scene.data.insert(uid, std::collections::HashMap::new());
                        }

                        let universe = scene.data.get_mut(&uid).unwrap();
                        universe.insert(item.index, multiverse.volatile[uid][item.index]);
                    }
                }

                multiverse.persistant.write();
                SnippetResult::ok(None)
            }
        }
    }
}

/* Functions */
fn run_scene(id: &str, duration: Option<u64>, multiverse: &mut Multiverse) -> bool {
    let scene = multiverse.persistant.scenes.get(id);
    if scene.is_none() {
        return false;
    }

    let scene = scene.unwrap();

    // no Fade Handler
    if duration.is_none() {
        for (uid, u) in &scene.data {
            let universe = &mut multiverse.volatile[*uid];
            for (i, b) in u {
                universe[*i] = *b;
            }
        }

        return true;
    }

    // Fade Handler (~186μs overhead per universe)
    let duration = std::time::Duration::from_millis(duration.unwrap_or(0));
    let deadline = std::time::Instant::now().add(duration);
    let mut start_values = mem::Scene {
        data: HashMap::new(),
    };

    loop {
        let now = std::time::Instant::now();
        let dif = (deadline - now).as_secs_f32();
        let percent = if dif <= 0.0 {
            1.0
        } else {
            1.0 - dif / duration.as_secs_f32()
        };

        for (uid, u) in &scene.data {
            let universe = &mut multiverse.volatile[*uid];
            if !start_values.data.contains_key(uid) {
                start_values.data.insert(*uid, HashMap::new());
            }

            let start_universe = start_values.data.get_mut(uid).unwrap();
            for (i, b) in u {
                if !start_universe.contains_key(i) {
                    start_universe.insert(*i, universe[*i]);
                }

                let i = *i;
                universe[i] = lerp(*start_universe.get(&i).unwrap(), *b, percent);
            }
        }

        if now >= deadline {
            break;
        }
    }

    true
}

fn lerp(f: u8, t: u8, p: f32) -> u8 {
    let f = f as f32;
    (f + p * (t as f32 - f)).floor() as u8
}
