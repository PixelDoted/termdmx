use crate::{
    dmx::{result::SnippetResult, Multiverse},
    dprint,
};

use super::Action;

#[derive(serde::Deserialize, Debug)]
pub enum Mode {
    /// Create or Update a device
    #[serde(rename = "insert")]
    Insert,

    /// Remove a device
    #[serde(rename = "remove")]
    Remove,

    /// Get a devices index, length and universe
    #[serde(rename = "get")]
    Get,
}

/* Structs */

#[derive(serde::Deserialize, Debug)]
pub struct Device {
    pub id: String,
    pub mode: Mode,
    pub insert: Option<Insert>,
}

#[derive(serde::Deserialize, Debug)]
pub struct Insert {
    pub index: usize,
    pub length: usize,
    pub universe: Option<usize>,
}

/* Impls */
impl Action for Device {
    /// Create/Update, Remove or Get a device
    fn run(&self, multiverse: &mut Multiverse) -> SnippetResult {
        match &self.mode {
            Mode::Insert => {
                if self.insert.is_none() {
                    dprint!(
                        "Device Insert: Insert object doesn't exsist {:?}",
                        self.insert
                    );
                    return SnippetResult::error(Some(super::INSERT_OBJECT_DOESNT_EXSIST));
                }

                let c = self.insert.as_ref().unwrap();
                if c.index >= 512 || c.length + c.index >= 512 {
                    dprint!(
                        "Device Insert: Index out of bounds {} >= 512 || {} >= 512",
                        c.index,
                        c.length + c.index,
                    );
                    return SnippetResult::error(Some(super::INDEX_OUT_OF_BOUNDS));
                }

                multiverse.persistant.add_device(
                    self.id.clone(),
                    c.index,
                    c.length,
                    c.universe.unwrap_or(0),
                );

                SnippetResult::ok(None)
            }
            Mode::Remove => {
                if !multiverse.persistant.devices.contains_key(&self.id) {
                    dprint!("Device Remove: Device doesn't exsist {}", self.id);
                    return SnippetResult::error(Some(super::DEVICE_DOESNT_EXSIST));
                }

                multiverse.persistant.remove_device(&self.id);
                SnippetResult::ok(None)
            }
            Mode::Get => {
                if !multiverse.persistant.devices.contains_key(&self.id) {
                    dprint!("Device Get: Device doesn't exsist {}", self.id);
                    return SnippetResult::error(Some(super::DEVICE_DOESNT_EXSIST));
                }

                let device = multiverse.persistant.devices.get(&self.id).unwrap();
                let json = serde_json::json!({"name": self.id, "index": device.index, "length": device.length, "universe": device.universe});
                SnippetResult::ok(Some(json))
            }
        }
    }
}
