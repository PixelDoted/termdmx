use crate::{
    dmx::{result::SnippetResult, Multiverse},
    dprint,
};

use super::Action;

/* Actions */
#[derive(serde::Deserialize, Debug)]
pub struct Set {
    pub index: usize,
    pub value: u8,
    pub universe: Option<usize>,
    pub device: Option<String>,
}

#[derive(serde::Deserialize, Debug)]
pub struct Get {
    pub index: usize,
    pub universe: Option<usize>,
    pub device: Option<String>,
}

/* Impls */
impl Action for Set {
    /// Set a byte at index in universe/device to value
    fn run(&self, multiverse: &mut Multiverse) -> SnippetResult {
        let (offset, length, universe) = if self.device.is_some() {
            let device = multiverse
                .persistant
                .devices
                .get(self.device.as_ref().unwrap());
            if device.is_none() {
                dprint!("Set: Device doesn't exsist {:?}", self.device);
                return SnippetResult::error(Some(super::DEVICE_DOESNT_EXSIST));
            }

            let device = device.unwrap();
            (device.index, device.length, device.universe)
        } else {
            (0, 512, self.universe.unwrap_or(0))
        };

        if self.index >= length {
            dprint!("Set: Index out of bounds {} >= {}", self.index, length);
            return SnippetResult::error(Some(super::INDEX_OUT_OF_BOUNDS));
        }

        multiverse.volatile[universe][self.index + offset] = self.value;
        SnippetResult::ok(Some(self.value.into()))
    }
}

impl Action for Get {
    /// Get a byte at index in universe/device
    fn run(&self, multiverse: &mut Multiverse) -> SnippetResult {
        let (offset, length, universe) = if self.device.is_some() {
            let device = multiverse
                .persistant
                .devices
                .get(self.device.as_ref().unwrap());
            if device.is_none() {
                dprint!("Get: Device doesn't exsist {:?}", self.device);
                return SnippetResult::error(Some(super::DEVICE_DOESNT_EXSIST));
            }

            let device = device.unwrap();
            (device.index, device.length, device.universe)
        } else {
            (0, 512, self.universe.unwrap_or(0))
        };

        if self.index >= length {
            dprint!("Get: Index out of bounds {} >= {}", self.index, length);
            return SnippetResult::error(Some(super::INDEX_OUT_OF_BOUNDS));
        }

        let output = multiverse.volatile[universe][self.index + offset].into();
        SnippetResult::ok(Some(output))
    }
}
