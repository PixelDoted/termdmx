use std::time::Duration;

#[derive(serde::Serialize)]
pub struct OverallResult {
    pub result: Result,
    pub time: String,
    pub snippets: Vec<SnippetResult>,
}

#[derive(serde::Serialize)]
pub struct SnippetResult {
    pub result: Result,
    pub value: Option<serde_json::Value>,
}

#[derive(serde::Serialize)]
pub enum Result {
    /// Successfully parsed/handled
    #[serde(rename = "ok")]
    Ok,

    /// Failed to parse/handle
    #[serde(rename = "error")]
    Error,
}

/* Impls */
impl OverallResult {
    /// Create a Successfully parsed result
    pub fn ok(snippets: Vec<SnippetResult>) -> OverallResult {
        OverallResult {
            result: Result::Ok,
            time: String::new(),
            snippets,
        }
    }

    /// Created a Failed parse result
    pub fn error() -> OverallResult {
        OverallResult {
            result: Result::Error,
            time: String::new(),
            snippets: Vec::new(),
        }
    }

    /// Format time to microseconds
    pub fn set_time(&mut self, time: Duration) {
        self.time = format!("{}μs", time.as_micros());
    }
}

impl SnippetResult {
    // Create a Successfully Handled result
    pub fn ok(value: Option<serde_json::Value>) -> SnippetResult {
        SnippetResult {
            result: Result::Ok,
            value,
        }
    }

    // Create a Failed Handle result
    pub fn error(value: Option<&str>) -> SnippetResult {
        let value: Option<serde_json::Value> = {
            match value {
                Some(e) => Some(serde_json::Value::String(String::from(e))),
                None => None,
            }
        };

        SnippetResult {
            result: Result::Error,
            value,
        }
    }
}
