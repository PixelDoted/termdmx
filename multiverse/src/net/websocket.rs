use futures_util::{SinkExt, StreamExt};
use warp::{
    ws::{Message, WebSocket},
    Filter, Reply,
};

use crate::dmx;

use super::util;

/// Handle websocket connections
pub fn warp() -> warp::filters::BoxedFilter<(impl Reply,)> {
    warp::any()
        .and(warp::header::header::<String>("content-type"))
        .and(warp::header::optional::<String>("authorization"))
        .and(warp::ws())
        .map(
            |content_type: String, auth: Option<String>, ws: warp::ws::Ws| {
                ws.on_upgrade(move |websocket| handle_websocket(websocket, content_type, auth))
            },
        )
        .boxed()
}

/// Handle websocket request
pub async fn handle_websocket(websocket: WebSocket, content_type: String, auth: Option<String>) {
    let (mut socket_tx, mut socket_rx) = websocket.split();
    if !util::is_authorized(auth) {
        // Check Authorization
        let _ = socket_tx.send(Message::text("not authorized"));
        let _ = socket_tx.close();
        return;
    }

    // get Content-Type
    let content_type = {
        let typ = util::get_content_type(content_type);
        if typ.is_none() {
            let _ = socket_tx.send(Message::text("invalid content type"));
            let _ = socket_tx.close();
            return;
        }

        typ.unwrap()
    };

    // Read requests
    while let Some(result) = socket_rx.next().await {
        let received_time = std::time::Instant::now();
        if result.is_err() {
            continue;
        }

        let msg = result.unwrap();
        let msg_binary = msg.as_bytes();

        // Handle request
        let request = content_type.from_slice::<dmx::Request>(msg_binary);
        let response = tokio::task::block_in_place(|| dmx::parse(request)); // parse request
        if response.is_none() {
            let _ = socket_tx
                .send(Message::text("Internal Error: Failed to handle request"))
                .await;
            eprintln!("Websocket: Failed to handle request");
            continue;
        }

        // set response time
        let mut response = response.unwrap();
        response.set_time(received_time.elapsed());

        // response to json or msgpack
        let response_bytes = content_type.to_slice(&response);
        if response_bytes.is_none() {
            let _ = socket_tx.send(Message::text("Internal Error: Failed to generate response"));
            eprintln!("Websocket: Failed to generate response");
            continue;
        }

        // send response
        let _ = socket_tx
            .send(Message::binary(response_bytes.unwrap()))
            .await; // send response
    }
}
