use once_cell::sync::OnceCell;
use warp::Filter;

mod http;
mod util;
mod websocket;

pub static CONFIG: OnceCell<util::Config> = OnceCell::new();

pub async fn handle_new(ip: &str, auth: &Option<String>, tls: Option<(&str, &str)>) {
    if CONFIG.set(util::Config::new(auth.clone())).is_err() {
        panic!("Failed to create Network Config");
    }
    let address = util::to_address(ip);

    // Services
    let websocket = websocket::warp();
    let http = http::warp();

    // Start server
    let server = warp::serve(warp::any().and(websocket.or(http)).boxed());
    if tls.is_some() {
        // Server TLS
        let tls = tls.unwrap();
        server
            .tls()
            .cert_path(tls.0)
            .key_path(tls.1)
            .run(address)
            .await;
    } else {
        // Server Plain
        server.run(address).await;
    };
}
