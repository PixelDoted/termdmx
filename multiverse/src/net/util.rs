use std::{net::SocketAddr, str::FromStr};

/// Authorization Check
pub fn is_authorized(auth: Option<String>) -> bool {
    let config = super::CONFIG.get().unwrap();
    config.auth.is_none()
        || auth.is_some() && config.auth.as_ref().unwrap() != auth.as_ref().unwrap()
}

/// convert String to SocketAddr
pub fn to_address(addr: &str) -> SocketAddr {
    let addr = SocketAddr::from_str(addr);
    if addr.is_err() {
        panic!("{}", addr.unwrap_err());
    }

    addr.unwrap()
}

/* Handle Json and MessagePack Serialization */
pub fn get_content_type(value: String) -> Option<ContentType> {
    match value.to_lowercase().as_str() {
        "application/json" => Some(ContentType::JSON),
        "application/msgpack" => Some(ContentType::MSGPACK),
        _ => None,
    }
}

/// Valid Content Types
pub enum ContentType {
    JSON,    // application/json
    MSGPACK, // application/msgpack
}

impl ContentType {
    /// deserialize bytes to T using Json or MsgPack
    pub fn from_slice<'a, T>(&self, bytes: &'a [u8]) -> Option<T>
    where
        T: serde::Deserialize<'a>,
    {
        match self {
            &ContentType::JSON => serde_json::from_slice::<T>(bytes).ok(),
            &ContentType::MSGPACK => rmp_serde::from_slice::<T>(bytes).ok(),
        }
    }

    /// serialize T using JSON or MsgPack to bytes
    pub fn to_slice<T>(&self, input: &T) -> Option<Vec<u8>>
    where
        T: serde::Serialize,
    {
        match self {
            &ContentType::JSON => serde_json::to_vec::<T>(input).ok(),
            &ContentType::MSGPACK => rmp_serde::to_vec::<T>(input).ok(),
        }
    }
}

/* Config */
pub struct Config {
    pub auth: Option<String>,
}

impl Config {
    pub fn new(auth: Option<String>) -> Config {
        Config {
            auth: match auth {
                Some(s) => Some(format!("Basic {}", s)),
                None => None,
            },
        }
    }
}
