use warp::{
    hyper::{body::Bytes, StatusCode},
    Filter, Reply,
};

use crate::dmx;

use super::util;

/// Handle http requests
pub fn warp() -> warp::filters::BoxedFilter<(impl Reply,)> {
    warp::any()
        .and(warp::header::header::<String>("content-type"))
        .and(warp::header::optional::<String>("authorization"))
        .and(warp::post())
        .and(warp::body::bytes())
        .map(|content_type: String, auth: Option<String>, body: Bytes| {
            let result = handle_http(
                String::from_utf8(body.to_vec()).unwrap(),
                content_type,
                auth,
            );
            if result.is_none() {
                return warp::reply::with_status("", StatusCode::INTERNAL_SERVER_ERROR)
                    .into_response();
            }

            let response = result.unwrap();
            response.into_response()
        })
        .boxed()
}

/// Handle http request
pub fn handle_http(body: String, content_type: String, auth: Option<String>) -> Option<Vec<u8>> {
    if !util::is_authorized(auth) {
        return Some("not authorized".as_bytes().to_vec());
    }

    // get Content-Type
    let content_type = {
        let typ = util::get_content_type(content_type);
        if typ.is_none() {
            return Some("invalid content type".as_bytes().to_vec());
        }

        typ.unwrap()
    };

    // parse request
    let received_time = std::time::Instant::now();
    let response = tokio::task::block_in_place(|| {
        dmx::parse(content_type.from_slice::<dmx::Request>(body.as_bytes()))
    });
    if response.is_none() {
        eprintln!("Http: Failed to handle request");
        return Some(
            "Internal Error: Failed to handle request"
                .as_bytes()
                .to_vec(),
        );
    }

    // set response time
    let mut response = response.unwrap();
    response.set_time(received_time.elapsed());

    // response to json or msgpack
    let response_bytes = content_type.to_slice(&response);
    if response_bytes.is_none() {
        eprintln!("Http: Failed to generate response");
        return Some(
            "Internal Error: Failed to generate response"
                .as_bytes()
                .to_vec(),
        );
    }

    // send response
    Some(response_bytes.unwrap())
}
