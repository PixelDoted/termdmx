use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    /// Enable debugging
    #[arg(short, long, default_value_t = false)]
    pub debug: bool,

    /// Initial universe count
    #[arg(short, long, default_value_t = 1)]
    pub initial_universes: usize,

    /// Use the data in exsisting Universe Memory-Mapped files
    #[arg(short, long, default_value_t = false)]
    pub use_mmaps: bool,

    /* Network */
    /// IP Address and Port
    #[arg(short, long, default_value_t = String::from("127.0.0.1:2233"))]
    pub address: String,

    /* TLS */
    #[command(subcommand)]
    pub tls: Option<SubTLS>,

    /// Authorization Header e.g. `user:pass`
    #[arg(short, long, default_value = None)]
    pub auth: Option<String>,
}

#[derive(Subcommand, Debug)]
pub enum SubTLS {
    TLS {
        /// TLS Certificate Path
        #[arg(short, long)]
        cert: String,

        /// TLS Key Path
        #[arg(short, long)]
        key: String,
    },
}

impl Cli {
    pub fn get_tls(&self) -> Option<(&str, &str)> {
        if self.tls.is_none() {
            return None;
        }

        match self.tls.as_ref().unwrap() {
            SubTLS::TLS { cert, key } => Some((cert, key)),
        }
    }
}
