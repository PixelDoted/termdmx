use clap::Parser;

mod cli;
mod dmx;
mod mem;
mod net;

pub const MEMMAP_PATH: &str = "./.memmap";
static mut PRINT_DEBUG: bool = false;

#[tokio::main]
async fn main() {
    let args = cli::Cli::parse();
    unsafe {
        PRINT_DEBUG = args.debug;
    }

    // check if the memmap folder exsists
    let memmap_folder = std::fs::read_dir(MEMMAP_PATH);
    if memmap_folder.is_err() {
        // create memory-mapped file directory
        let _ = std::fs::create_dir(MEMMAP_PATH);
    } else if !args.use_mmaps {
        // blackout all bytes in pre-generated mmap files
        for e in memmap_folder.unwrap() {
            if e.is_err() {
                continue;
            }

            let _ = std::fs::write(e.unwrap().path(), &[0; 512]);
        }
    }

    // create multiverse memory
    unsafe {
        let _ = dmx::MULTIVERSE.set(dmx::Multiverse {
            volatile: mem::Volatile::new(args.initial_universes),
            persistant: mem::Persistant::new(),
        });
    }

    println!("Started Multiverse on '{}'", args.address);

    // start network
    let _ = net::handle_new(&args.address, &args.auth, args.get_tls()).await;
}

#[macro_export]
macro_rules! dprint {
    ($($arg:tt)*) => {{
        unsafe {
            if crate::PRINT_DEBUG {
                println!($($arg)*);
            }
        }
    }};
}
