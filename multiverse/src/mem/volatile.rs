use std::{
    collections::HashMap,
    ops::{Index, IndexMut},
};

/// Cleared on Exit
pub struct Volatile {
    pub universes: HashMap<usize, memmap2::MmapMut>,
}

/* Impls */
impl Volatile {
    pub fn new(count: usize) -> Volatile {
        let mut universes = HashMap::new();
        for i in 0..count {
            let file = new_memmap(i);
            if file.is_none() {
                panic!();
            }

            universes.insert(i, file.unwrap());
        }

        Volatile { universes }
    }
}
impl Index<usize> for Volatile {
    type Output = [u8];

    /// Index into universe
    fn index(&self, index: usize) -> &Self::Output {
        let u = self.universes.get(&index);
        if u.is_none() {
            return &[0; 512];
        }

        u.unwrap()
    }
}
impl IndexMut<usize> for Volatile {
    // Index into and/or create universe
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        if !self.universes.contains_key(&index) {
            let mmap = new_memmap(index);
            self.universes.insert(index, mmap.unwrap());
        }

        return self.universes.get_mut(&index).unwrap();
    }
}

/* Functions */
pub fn new_memmap(index: usize) -> Option<memmap2::MmapMut> {
    let file = std::fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(&format!("{}/{}-mmap", crate::MEMMAP_PATH, index));
    if file.is_err() {
        eprintln!("{}", file.unwrap_err());
        return None;
    }

    let file = file.unwrap();
    let _ = file.set_len(512);
    let mmap = unsafe { memmap2::MmapMut::map_mut(&file) };
    if mmap.is_err() {
        eprintln!("{}", mmap.unwrap_err());
        return None;
    }

    mmap.ok()
}
