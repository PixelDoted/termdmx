use configuro::rmp_serde;
use std::collections::HashMap;

/// Stored on Disk
#[derive(serde::Deserialize, serde::Serialize, Default, configuro::Data)]
pub struct Persistant {
    pub devices: HashMap<String, Device>,
    pub scenes: HashMap<String, Scene>,
}

#[derive(serde::Deserialize, serde::Serialize)]
pub struct Device {
    pub index: usize,
    pub length: usize,
    pub universe: usize,
}

#[derive(serde::Deserialize, serde::Serialize, Debug)]
pub struct Scene {
    pub data: HashMap<usize, HashMap<usize, u8>>,
}

/* Impls */
impl Persistant {
    pub fn new() -> Persistant {
        Persistant::read()
    }

    /// Create a device
    pub fn add_device(&mut self, id: String, index: usize, length: usize, universe: usize) {
        self.devices.insert(
            id,
            Device {
                index,
                length,
                universe,
            },
        );
        self.write();
    }

    // Create a scene
    pub fn add_scene(&mut self, id: String) {
        self.scenes.insert(
            id,
            Scene {
                data: HashMap::new(),
            },
        );
        self.write();
    }

    // Remove a device
    pub fn remove_device(&mut self, id: &str) {
        self.devices.remove(id);
        self.write();
    }

    // Remove a scene
    pub fn remove_scene(&mut self, id: &str) {
        self.scenes.remove(id);
        self.write();
    }
}
